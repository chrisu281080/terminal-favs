# terminal-favs

This a compact GUI to execute favorite terminal commands. It’s designed to be used for the Librem5 (or other Linux Smartphones with GTK). But it can also be used on normal screens.
Some examples are availble for download but you can directly save all your personal favorites.

## Examples
Just copy them into "~/.terminal-favs/" to use them directly.
- Librem5 Scaling [requires "apt install wlr-randr"]
- SSH [requires "apt install ssh"]
- Take screenshot (in 5 sec) [requires "apt install grim"]
- WlanScan [requires "apt install network-manager"]


## License
### GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)
