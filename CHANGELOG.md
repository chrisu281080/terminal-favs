## V0.2 (2021-06-04)
- Changed from os.popen to subprocess
- Added popup for STDOUT / STDERR for just-execute

## V0.1 (2021-01-01)
- Initial release
